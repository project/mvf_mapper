Measured Value Field feeds mapper
=================================

This module integrates Measured Value Field [0] with Feeds [1]. It provides three
mappers to import Measured Value Fields with feeds: one mapper for the from
value, one for the to value and one for the units. The units mapper expects the
symbol of the unit, ie. mm for milimiter or h for hours.

[0] http://drupal.org/project/mvf
[1] http://drupal.org/project/feeds
